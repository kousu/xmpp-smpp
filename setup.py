from setuptools import setup, find_packages
import pathlib


here = pathlib.Path(__file__).parent.resolve()

setup(
  name='xmpp-smpp',
  version='0.0.1',
  description='Bridge SMPP to a XMPP XEP 0114 transport, i.e. send SMSes through your jabber server.',
  long_description=(here / 'README.md').read_text(encoding='utf-8'),
  long_description_content_type='text/markdown',
  author='awooooo',
  author_email='example.com',
  license='MIT',
  py_modules=[
    'xmpp_smpp',
  ],
  # if we grow, move everything under src/dupes/ and make that a real package
  # for now, py_modules is enough.
  #packages=find_packages(),
  python_requires='>=3.6,<=3.10', # TODO: actually test these
  install_requires=[
    'slixmpp>=1',
    'smpplib>=2'
  ],
  entry_points={
    'console_scripts': [
      'xmpp-smpp = xmpp_smpp:main',
    ],
  },
)

