# XMPP-SMPP Bridge

An XMPP [transport](https://xmpp.org/extensions/xep-0100.html) [component](https://xmpp.org/extensions/xep-0114.html)
which bridges out to an [SMPP](https://smpp.org/#protocoloperationsandpdus) server. In other words, it can send texts
between your jabber account and cellphones.


## Install

```
# because I am a hobo
pip install -e .
```


## Usage

Setting this up is a bit involved. You need to have an XMPP server,
it needs to have external components enabled (prosody: https://prosody.im/doc/components#adding_an_external_component, ejabberd: ??? [apparently supported](https://docs.ejabberd.im/developer/hosts/) but undocumented??),
and you need an account with an SMPP server.

*and* you need to set up a map of jids<->numbers (this part is pretty..uh..missing in the code ATM.).
In a real working transport, you would probably have to have an accounts database attached with its own registration system outside of XMPP.

As an a sysadmin, you give it a (sub)domain on your xmpp server, say, `sms.example.org`.

Then, as a user, you need to sinscribe to bind your JID to a phone number,
but then to send a text to, say, tel:+1(666)555-1234, you simply message with xmpp:+16665551234@sms.example.org.


### Example

```
$ xmpp-smpp -d -j bots.kousu.ca --smpplogin isufdhds
XMPP Password: 
SMPP Password: 
DEBUG    Using selector: EpollSelector
DEBUG    Loaded Plugin: XEP-0030: Service Discovery
DEBUG    Connecting to localhost:5347
DEBUG    Event triggered: connecting
INFO     Connecting to localhost:2775...
DEBUG    DNS: Querying localhost for AAAA records.
DEBUG    Receiver mode
DEBUG    Sending bind_transceiver PDU
DEBUG    >>b'000000260000000900000000000000016973756664686473006664736f69686a000034000000' (38 bytes)
DEBUG    Waiting for PDU...
DEBUG    DNS: Querying localhost for A records.
DEBUG    Event triggered: connected
DEBUG    SEND: <stream:stream xmlns="jabber:component:accept" xmlns:stream="http://etherx.jabber.org/streams" to="bots.kousu.ca">
DEBUG    RECV: <stream:stream xml:lang="en" id="7915a2b7-f2d2-42d6-96a5-91ae520a3413" from="bots.kousu.ca">
DEBUG    SEND: <handshake xmlns="jabber:component:accept">04f7913506d935737081f9742c8967693cffb002</handshake>
DEBUG    RECV: <handshake />
DEBUG    Event triggered: session_bind
DEBUG    Event triggered: session_start
DEBUG    Event triggered: sent_presence
DEBUG    SEND: <presence id="a32b18b8a7f24e9a893d12d0f541011a" from="bots.kousu.ca" />
DEBUG    RECV: <presence to="bots.kousu.ca" type="error" id="a32b18b8a7f24e9a893d12d0f541011a"><error type="modify"><bad-request xmlns="urn:ietf:params:xml:ns:xmpp-stanzas" /><text xmlns="urn:ietf:params:xml:ns:xmpp-stanzas">Components MUST specify a &apos;to&apos; address on stanzas</text></error></presence>
DEBUG    Event triggered: presence
DEBUG    Event triggered: presence_error
```

...


### Known Carriers

Not all (e.g. Twilio doesn't; it's unclear to me if [Bandwidth does](https://www.bandwidth.com/glossary/short-message-peer-to-peer-smpp/))

* [Clickatell](https://www.clickatell.com/)
* [VoIP.ms](https://wiki.voip.ms/article/SMPP)
  - uh, but they need you to run the SMPP server too?

There's also these SMPP servers you can run yourself:

* https://github.com/fizzed/cloudhopper-smpp/tree/master/src/main/java/com/cloudhopper/smpp/simulator
* https://www.auronsoftware.com/kb/sms-server/how-to/setup-secure-smpp-server/#setup_and_configure
* https://camel.apache.org/components/3.4.x/smpp-component.html
* https://github.com/ajankovic/smpp
