#!/usr/bin/env python3
"""
A transport component in aioxmpp

This seems to be unsupported at the moment: https://github.com/horazont/aioxmpp/issues/73
so forget aioxmpp for now

(also after reading through it's docs..it really really seems overengineered.
 it has wayyyyy too many nested options and configurator classes all over the damn place. 
 and 
 i like the way they write; that seems nicer than slixmpp's arcane...thing..that they do.)
"""

import asyncio
import aioxmpp


import logging

log = logging.getLogger(__name__)
log.setLevel(logging.DEBUG)

async def main(jid, password):
    logging.getLogger("aioxmpp.XMLStream").setLevel(logging.DEBUG)

    client = aioxmpp.Client(
        aioxmpp.JID.fromstr(jid),
        aioxmpp.make_security_layer(password),
        override_peer=[('comms2.kousu.ca', 5347, aioxmpp.connector.STARTTLSConnector())],
        logger=log,
    )
    

    def message_received(msg):
        if not msg.body:
            # do not reflect anything without a body
            return

        # we could also use reply = msg.make_reply() instead
        reply = aioxmpp.Message(
            type_=msg.type_,
            to=msg.from_,
        )

        # make_reply() would not set the body though
        reply.body.update(msg.body)

        client.enqueue(reply)

    message_dispatcher = client.summon(
        aioxmpp.dispatcher.SimpleMessageDispatcher
    )
    message_dispatcher.register_callback(
        aioxmpp.MessageType.CHAT,
        None,
        message_received,
    )

    async with client.connected():
        print("connected")
        while True:
            await asyncio.sleep(1)


if __name__ == "__main__":
    jid = 'bots.kousu.ca'
    password = 'xxxgggg' #getpass.getpass()

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(jid, password))
    loop.close()
