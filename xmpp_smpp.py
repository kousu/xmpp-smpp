#!/usr/bin/env python3

import threading
from getpass import getpass
from argparse import ArgumentParser
import logging

import slixmpp
from slixmpp.componentxmpp import ComponentXMPP

import smpplib


class TransportComponent(ComponentXMPP):
    GATEWAY_TYPE = None # should be overriden by a subclass
    
    def __init__(self, jid, secret, server='localhost', port=5437, desc=None):
        super().__init__(jid, secret, server, port)

        self.auto_authorize = True
        
        # Advertise ourselves as a Transport
        # - only some clients (e.g. Gajim) recognize and handle this
        self.register_plugin('xep_0030')
        self['xep_0030'].add_identity(category='gateway',
                      itype=self.GATEWAY_TYPE,
                        # yes, that's a unicode arrow to represent bridging
                      name=desc if desc else '',
                      jid=self.boundjid,
                      lang='en',
                    )
        self['xep_0030'].add_item(self.boundjid, ijid=self.boundjid)

        # TODO: is this necessary? I feel like there was some something that auto-handled presence already.
        self.add_event_handler("session_start", lambda *args: self.send_presence())


class XMPP_SMPP_Transport(TransportComponent):
    GATEWAY_TYPE = "sms"


class Bridge:
    def __init__(self, xmpp_jid, xmpp_password, xmpp_server, xmpp_port, smpp_login, smpp_password, smpp_server, smpp_port):
        self._smpp_login = smpp_login
        self._smpp_password = smpp_password

        # sockets
        self.xmpp = XMPP_SMPP_Transport(xmpp_jid, xmpp_password, xmpp_server, xmpp_port)
        self.smpp = smpplib.client.Client(smpp_server, smpp_port)

        # event handlers
        self.xmpp.add_event_handler("message", self.on_xmpp_message)
        self.smpp.set_message_received_handler(self.on_smpp_message)

    def connect(self):
        self.xmpp.connect()

        # we *don't* connect to smpp here because smpplib wrote that in a blocking way
        # while slixmpp just queues the connection request for when process() is called later
        # so if we tried, and it fails we deadlock
    
    def process(self):
        # TODO: threading isn't great because it's hard to make it quit
        #       maybe there's a way to make smpplib more async-friendly?
        def _s():
            self.smpp.connect()
            self.smpp.bind_transceiver(system_id=self._smpp_login, password=self._smpp_password)
            self.smpp.listen()
        threading.Thread(target=_s).start()

        self.xmpp.process()
    
        # TODO: this is a perfect case to try out https://trio.readthedocs.io/en/stable/ ( https://vorpus.org/blog/notes-on-structured-concurrency-or-go-statement-considered-harmful/ )

    def on_xmpp_message(self, msg):
        # translate xmpp -> smpp
        number_from = registered_accounts[msg['from'].bare]
        number_from = bytes(number_from, 'ascii')
        body = msg['body']
        number_to = msg['to'].username # or something
        number_to = bytes(number_to, 'ascii')
        
        assert type(body) is unicode # or just 'str' in python3
        parts, encoding_flag, msg_type_flag = smpplib.gsm.make_parts(body)
        for pkt in parts:
            pdu = self.smpp.send_message(
                source_addr_ton=smpplib.consts.SMPP_TON_INTL,
                #source_addr_npi=smpplib.consts.SMPP_NPI_ISDN,
                # Make sure it is a byte string, not unicode:
                source_addr=number_from,

                dest_addr_ton=smpplib.consts.SMPP_TON_INTL,
                #dest_addr_npi=smpplib.consts.SMPP_NPI_ISDN,
                # Make sure thease two params are byte strings, not unicode:
                destination_addr=number_to,
                short_message=part,

                data_coding=encoding_flag,
                esm_class=msg_type_flag,
                registered_delivery=True,
            )
            logging.debug(f"SEND: {pdu.sequence}")

            # TODO: you should make sure to catch errors (e.g. if `submit_sm_resp` doesn't show up on time)
            #       and return them as <iq type='error'> stanzas to the XMPP side.

    def on_smpp_message(self, pdu):
        jid_from = f'{pdu.source_addr}@{self.xmpp.boundjid}'
        jid_to = registered_accounts[pdu.destination_addr]
        body = pdu.short_message
        # TODO: handle multipart messages
        #       these are message
        if pdu.more_messages_to_send:
            raise NotImplemented
        # there are many more options you might try to interpret;
        # see https://github.com/python-smpplib/python-smpplib/blob/51a5375a95fa16fa84f0eae5d721e861cc291504/smpplib/command.py#L757
        self.xmpp.send_message(jid_to, body, mfrom=jid_from, mtype='chat')
        
def main():
    # Setup the command line arguments.
    parser = ArgumentParser()

    # Output verbosity options.
    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
                        action="store_const", dest="loglevel",
                        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)

    # JID and password options.
    parser.add_argument("-j", "--jid",
                        required=True,
                        help="JID to use")
    parser.add_argument("-p", "--xmpppassword",
                        help="xmpp component password to use")
    parser.add_argument("-s", "--xmppserver",
                        default='localhost',
                        help="xmpp server to connect to")
    parser.add_argument("-P", "--xmppport",
                        default=5347,
                        help="port to connect to")

    # SMPP connection
    parser.add_argument("-l", "--smpplogin",
                        required=True,
                        help="smpp login to use")
    parser.add_argument("-m", "--smpppassword",
                        help="smpp password to use")
    parser.add_argument("-n", "--smppserver",
                        default='localhost',
                        help="smpp server to connect to")
    parser.add_argument("-a", "--smppport",
                        default=2775, # from /etc/services
                        help="port to connect to smpp at")

    args = parser.parse_args()

    if args.xmpppassword is None:
        args.xmpppassword = getpass("XMPP Password: ") # or via en envvar?
    if args.smpppassword is None:
        args.smpppassword = getpass("SMPP Password: ") # or via en envvar?

    args.xmpport = int(args.xmppport)
    args.smpport = int(args.smppport)

    # Setup logging.
    logging.basicConfig(level=args.loglevel,
                        format='%(levelname)-8s %(message)s')

    # TODO: should the bridge own the connections, or should it just be passed them in?
    bridge = Bridge(args.jid, args.xmpppassword, args.xmppserver, args.xmppport,
                    args.smpplogin, args.smpppassword, args.smppserver, args.smppport) 
    bridge.connect()
    bridge.process()


if __name__ == '__main__':
    main()
